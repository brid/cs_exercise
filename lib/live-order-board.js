'use strict';

// Simple array to hold orders.
// In reality you would have some kind of db context instance or abstracting repository module
let orders = [];

module.exports = {
	addOrder: function (newOrder) {
		if (newOrder instanceof this.Order) {
			const orderId = generateGuid();
			newOrder.orderId = orderId;
			orders.push(newOrder);
			return orderId;
		}
		// Fall through
		throw new Error('A valid order object must be passed');
	},
	cancelOrder: function (orderId) {
		// Any other validation business logic here.
		if (orderId) {
			let before = orders.length;
			orders = orders.filter(function (theOrder) {
				return theOrder.orderId !== orderId;
			});
			return orders.length === (before - 1);
		}
		// Fall through
		throw new Error('Order id must be passed');
	},
	getSummaryInformation: function () {
		// Filter our orders into BUY and SELL
		const buyOrders = orders.filter(function (theOrder) {
			return theOrder.orderType !== 'SELL';
		});
		const sellOrders = orders.filter(function (theOrder) {
			return theOrder.orderType !== 'BUY';
		});
		// Group these buy order price
		const groupedBuyOrders = getGrouped(buyOrders);
		const groupedSellOrders = getGrouped(sellOrders);
		// Transform into our desired format
		const summarisedBuyOrders = getSummary(groupedBuyOrders, 'BUY').sort(function (a, b) {
			return a.pricePerKilogram > b.pricePerKilogram;
		});
		const summarisedSellOrders = getSummary(groupedSellOrders, 'SELL').sort(function (a, b) {
			return a.pricePerKilogram < b.pricePerKilogram;
		});
		return summarisedSellOrders.concat(summarisedBuyOrders);
	},
	Order: function (userId, orderQuantity, pricePerKilogram, orderType) {
		// Any other validation business logic here.
		if (userId && orderQuantity && pricePerKilogram && orderType) {
			this.userId = userId;
			this.orderQuantity = orderQuantity;
			this.pricePerKilogram = pricePerKilogram;
			this.orderType = orderType;
		} else {
			throw new Error('Please specify all values');
		}
	}
};

function generateGuid() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		s4() + '-' + s4() + s4() + s4();
}

function getSummary(groupedOrders, orderType) {
	return Object.keys(groupedOrders).map(function (key) {
		return {orderType: orderType, orderQuantity: parseFloat(key), pricePerKilogram: groupedOrders[key].reduce(function (a, b) {
			return a + b;
		})
		};
	});
}

function getGrouped(filteredOrders) {
	return filteredOrders.reduce(function (obj, item) {
		obj[item.pricePerKilogram] = obj[item.pricePerKilogram] || [];
		obj[item.pricePerKilogram].push(item.orderQuantity);
		return obj;
	}, {});
}

