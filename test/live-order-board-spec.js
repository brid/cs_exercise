const expect = require('chai').expect;
const rewire = require('rewire');

const liveOrderBoard = rewire('../lib/live-order-board');
describe('live order board', function () {
	describe('adding a new order', function () {
		beforeEach(function () {
			liveOrderBoard.__set__('orders', []);
		});
		it('should register a new order', function () {
			const newOrder = new liveOrderBoard.Order('user5', 3.5, 606, 'BUY');
			const createOrder = liveOrderBoard.addOrder(newOrder);
			const getOrders = liveOrderBoard.getSummaryInformation();

			expect(createOrder).to.not.be.null;
			expect(getOrders.length).to.equal(1);
		});
		it('should throw an error if an invalid order is given', function () {
			expect(function () {
				liveOrderBoard.addOrder(null);
			}).to.throw('A valid order object must be passed');
		});
		it('should throw an error if a malformed order is given', function () {
			expect(function () {
				const newOrder = new liveOrderBoard.Order('user6', 3.5, 303);
				liveOrderBoard.addOrder(newOrder);
			}).to.throw('Please specify all values');
		});
	});
	describe('cancelling an existing order', function () {
		beforeEach(function () {
			liveOrderBoard.__set__('orders', []);
		});
		it('should cancel an existing order', function () {
			const newOrder = new liveOrderBoard.Order('user7', 3.5, 909, 'BUY');
			const createOrder = liveOrderBoard.addOrder(newOrder);
			const cancelResponse = liveOrderBoard.cancelOrder(createOrder);
			const getOrders = liveOrderBoard.getSummaryInformation();

			expect(cancelResponse).to.be.true;
			expect(getOrders.length).to.equal(0);
		});
		it('should throw an error if an invalid order id is given', function () {
			expect(function () {
				liveOrderBoard.cancelOrder(null);
			}).to.throw('Order id must be passed');
		});
	});
	describe('get summary information of live orders', function () {
		beforeEach(function () {
			liveOrderBoard.__set__('orders', []);
		});
		it('should show summary information in the right format', function () {
			// Sell orders
			liveOrderBoard.addOrder(new liveOrderBoard.Order('user1', 3.5, 306, 'SELL')); // order a
			liveOrderBoard.addOrder(new liveOrderBoard.Order('user2', 1.2, 310, 'SELL')); // order b
			liveOrderBoard.addOrder(new liveOrderBoard.Order('user3', 1.5, 307, 'SELL')); // order c
			liveOrderBoard.addOrder(new liveOrderBoard.Order('user4', 2.0, 306, 'SELL')); // order d
			// Buy orders
			liveOrderBoard.addOrder(new liveOrderBoard.Order('user1', 3.5, 245, 'BUY'));
			liveOrderBoard.addOrder(new liveOrderBoard.Order('user2', 1.2, 302, 'BUY'));
			liveOrderBoard.addOrder(new liveOrderBoard.Order('user3', 1.5, 245, 'BUY'));
			liveOrderBoard.addOrder(new liveOrderBoard.Order('user4', 2.0, 302, 'BUY'));

			const orderSummaries = liveOrderBoard.getSummaryInformation();

			// Ensure we have some order summaries returned
			expect(orderSummaries).to.not.be.null;

			// We should have exactly 5 summaries
			expect(orderSummaries.length).to.equal(5);

			// Our sell orders
			expect(orderSummaries[0].orderQuantity).to.equal(306); // order a + order d
			expect(orderSummaries[1].orderQuantity).to.equal(307); // order c
			expect(orderSummaries[2].orderQuantity).to.equal(310); // order b

			// Our buy orders
			expect(orderSummaries[3].orderQuantity).to.equal(302);
			expect(orderSummaries[4].orderQuantity).to.equal(245);
		});
	});
});
